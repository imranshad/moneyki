import React,{useState} from 'react';
import { Text, View, StyleSheet, Appearance,TouchableOpacity, Image } from 'react-native';
import { colors } from 'services/Theme';
import { plus_circle, minus_circle, gold } from 'assets/icons';
import { TextInput } from 'react-native-gesture-handler';

const ListItem = ({
    title,
    desc,
    price,
    weight,
    unallowcated
}) =>{
    const [count,setCount]=useState(0)
    return(
    <View style={styles.container}>
        <Image source={gold} resizeMode="contain" />
        <View style={{marginLeft:10,marginRight:40}}>
            <Text style={styles.tag1}>{title}</Text>
            <Text style={styles.tag2}>{desc}</Text>
            <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
                <Text style={styles.tag3}>EUR {price}</Text>
                {unallowcated ?
                <View style={{flexDirection:"row",alignItems:"center",margin:10}}>
                <Text style={{color:colors.grey,fontFamily:"Roboto-Medium"}}>WEIGHT :</Text>
                <TextInput
                keyboardType="number-pad"
                style={styles.entWeight}
                placeholderTextColor={Appearance.getColorScheme()=="dark"?colors.black20:"#DDDDDD"}
                    placeholder="Enter weight "
                />
                </View>
                :
                <View style={{marginRight:20}}>
                <Text style={styles.tag4}>WEIGHT: {weight}</Text>
                <Text style={styles.g}>g</Text>
                </View>
                }
            </View>
            {!unallowcated &&
            <View style={styles.btnCont}>
                <TouchableOpacity 
                style={styles.counterIco}
                onPress={()=>setCount(count+1)}>
                    <Image source={plus_circle} resizeMode="contain"/>
                </TouchableOpacity>
                <Text style={styles.count}>{count}</Text>
                <TouchableOpacity 
            style={styles.counterIco}
            onPress={()=>setCount(count>0?count-1:0)}>
                    <Image source={minus_circle} resizeMode="contain" />
                </TouchableOpacity>
            </View>
            }
        </View>
    </View>
  );}

  export default ListItem
  const styles=StyleSheet.create({
      container:{
          flexDirection:"row",
          padding:20,
      },
      tag1:{
          fontFamily:"Roboto-Medium",
          fontSize:14,
          color:Appearance.getColorScheme()=="dark"?colors.white:colors.black
      },
      tag2:{
          fontFamily:"Roboto-Regular",
          color:colors.grey,
          fontSize:13,
          paddingRight:20,
          paddingBottom:20,
          marginTop:7
      },
      tag3:{
        color:colors.primary,
        fontFamily:"Roboto-Medium",
        fontSize:14
      },
      tag4:{
        color:colors.grey,
        fontFamily:"Roboto-Medium",
        fontSize:14,
        marginRight:10
      },
      entWeight:{
        backgroundColor:Appearance.getColorScheme()=="dark"?colors.black10:colors.sfotLight,
        fontFamily:"Roboto-Regular",
        fontSize:14,
        padding:0,
        paddingLeft:5,
        paddingRight:5,
        marginLeft:10,
        marginRight:10,
        borderRadius:5,
        height:30,
        color:colors.black20,
        width:90
      },
      btnCont:{
          flexDirection:"row",
          width:"50%",
          justifyContent:"space-between",
          borderWidth:1.5,
          borderColor:colors.primary,
          borderRadius:30,
          paddingLeft:0.5,
          paddingRight:0.5,
          marginTop:20
      },
      g:{
        color:colors.grey,
        fontFamily:"Roboto-Medium",
        fontSize:14,
        position:"absolute",
        right:0,top:6,
        textAlign:"right"
        },
      count:{
          alignSelf:"center",
          color:colors.grey,
          fontFamily:"Roboto-Regular",
          fontSize:15,
          borderColor:colors.primary
        },
      counterIco:{
          padding:10,
          borderRadius:20,
          alignItems:"center",
          justifyContent:"center",
          backgroundColor:Appearance.getColorScheme()=="dark"?colors.black10:colors.primaryLight
      }
  })