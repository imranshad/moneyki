import React from 'react';
import { Text, View, StyleSheet,Image, Appearance, TouchableOpacity } from 'react-native';
import { colors } from 'services/Theme';
import {arrow_left,arrow_left_white} from 'assets/icons'
const Header = ({
    title,
    navigation,
    style,
    iconStyle,
    RightBtn
}) => (
    <View style={[styles.container,style]}>
        {navigation.canGoBack() &&
        <TouchableOpacity style={[styles.icon,iconStyle]} onPress={()=>navigation.goBack()}>
        <Image style={styles.icon} source={Appearance.getColorScheme()=="dark"?arrow_left_white:arrow_left} resizeMode="contain" />
        </TouchableOpacity>
        }
        <Text style={styles.title}>{title}</Text>
        {RightBtn &&<RightBtn />}
    </View>
);

export default Header;
const styles=StyleSheet.create({
    container:{
        backgroundColor:Appearance.getColorScheme()=="dark"?colors.black:colors.background,
        width:"100%",
        height:60,
        flexDirection:"row",
        top:0,
        alignItems:"center",
    },
    title:{
        fontFamily:"Roboto-Medium",
        fontSize:16,
        flex:1,
        textAlign:"center",
        alignSelf:"center",
        justifyContent:"center",
        color:Appearance.getColorScheme()=="dark"?colors.white:colors.black
    },
    icon:{
        width:25,
        height:25,
        alignSelf:"center",
        position:"absolute",
        marginLeft:1
    }
})