import React from 'react';
import { Text,Image, View, StyleSheet } from 'react-native';
import { colors } from 'services/Theme';
import { calendar } from 'assets/icons';

const ListHead = ({
    date,
    balance
}) => (
    <View style={styles.container}>
        <View style={{flexDirection:"row",alignItems:"center"}}>
            <Image source={calendar} resizeMode="contain" />
            <Text style={styles.text}>{date}</Text>
        </View>
        <Text style={styles.text}>Balance: MKI {balance}</Text>
    </View>
);

export default ListHead;
const styles=StyleSheet.create({
    container:{
        borderColor:colors.primary,
        width:"100%",
        paddingLeft:40,
        paddingRight:40,
        paddingTop:5,
        paddingBottom:5,
        justifyContent: 'space-between',
        alignItems:"center",
        borderTopWidth:1,
        borderBottomWidth:1,
        flexDirection:"row",
        marginTop:20
    },
    text:{
        fontFamily:"Roboto-Medium",
        fontSize:15,
        margin:10,
        color:colors.primary
    }
})