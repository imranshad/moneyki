import React from 'react';
import { Text, View, StyleSheet, Image, Appearance } from 'react-native';
import { clock, arrow_up_right_red, arrow_down_left_green } from 'assets/icons'
import { colors } from 'services/Theme';
const ListItem = ({
    title,
    fee,
    time,
    mki,
    arrowRed
}) => (
    <View style={styles.container}>
        <Image source={arrowRed?arrow_up_right_red:arrow_down_left_green} resizeMode="contain" />
        <View>
            <Text style={styles.tag1}>{title}</Text>
            <Text style={styles.tag2}>Transfer Fee: {fee}</Text>
            <View style={{flexDirection:"row",alignItems:"center"}}>
                <Image source={clock} resizeMode="contain" />
                <Text style={styles.tag3}>{time}</Text>
            </View>
        </View>
        <View style={{flexDirection:"row",alignItems:"center"}}>
        <Text style={[styles.tag4,arrowRed&&{color:colors.cgreen}]}>{mki}</Text>
        <Text style={[styles.tag5,{marginBottom:-7,marginLeft:2}]}>MKI</Text>
        </View>
    </View>
);

export default ListItem;
const styles=StyleSheet.create({
    container:{
        flexDirection:"row",
        justifyContent:"space-between",
        marginLeft:30,
        marginRight:30,
        paddingTop:20,
        paddingBottom:20,
        alignItems:"center"
    },
    tag1:{
        fontFamily:"Roboto-Medium",
        fontSize:14,
        color:Appearance.getColorScheme()=="dark"?colors.white:colors.black
    },
    tag2:{
        color:colors.grey,
        fontFamily:"Roboto-Regular",
        fontSize:13,
        margin:5
    },
    tag3:{
        color:colors.grey,
        fontFamily:"Roboto-Regular",
        margin:5,
        fontSize:13
    },
    tag4:{
        fontFamily:"Roboto-Regular",
        fontSize:20,
        color:colors.lightRed,
        margin:5
    },
    tag5:{
        color:colors.grey,
        fontFamily:"Roboto-Regular",
        fontSize:10,
        margin:5
    },
})