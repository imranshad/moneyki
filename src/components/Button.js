import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { colors } from 'services/Theme';

const Button = ({
    children,
    onPress,
    style
}) => (
    <TouchableOpacity 
    onPress={onPress}
    style={[styles.container,style]}>
        {children}
    </TouchableOpacity>
);

export default Button;
const styles=StyleSheet.create({
    container:{
        borderWidth:3,
        borderColor:colors.grey,
        height:60,
        borderRadius:5,
        alignItems:"center",
        justifyContent:"center"
    }
})