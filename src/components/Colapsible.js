import React, { useRef } from "react";
import { View, Animated, ScrollView, Text,StyleSheet,Appearance, ImageBackground } from "react-native";
import { colors } from "services/Theme";
import { headerbg,headerbgp } from "assets/images";

const H_MAX_HEIGHT = 100;
const H_MIN_HEIGHT = 60;
const H_SCROLL_DISTANCE = H_MAX_HEIGHT - H_MIN_HEIGHT;
const CollapsibleHeader = ({children}) => {
  const scrollOffsetY = useRef(new Animated.Value(0)).current;
  const headerScrollHeight = scrollOffsetY.interpolate({
    inputRange: [0, H_SCROLL_DISTANCE],
    outputRange: [H_MAX_HEIGHT, H_MIN_HEIGHT],
    extrapolate: "clamp"
  });

  const textScrolLeft = scrollOffsetY.interpolate({
    inputRange: [0, 40],
    outputRange: [20, 580],
    extrapolate: "clamp",
    easing:(n)=>n*0.5,
    useNativeDriver: true
  });
  
  const textTopMargin = scrollOffsetY.interpolate({
    inputRange: [0, 40],
    outputRange: [40, 0],
    extrapolate: "clamp",
    useNativeDriver: true
  });
  const fontSizeInterpolate = scrollOffsetY.interpolate({
    inputRange: [20, 30],
    outputRange: [30, 20],
    extrapolate: "clamp",
    useNativeDriver: true
  });
  const ATFONT = scrollOffsetY.interpolate({
    inputRange:  [15,20],
    outputRange: [15,20],
    extrapolate: "clamp",
    useNativeDriver: true
  });
  const aTotalTop = scrollOffsetY.interpolate({
    inputRange: [0, 60],
    outputRange: [10, 15],
    extrapolate: "clamp",
    useNativeDriver: true
  });

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        onScroll={Animated.event([
            { nativeEvent: { contentOffset: { y: scrollOffsetY } } }
          ])}
        scrollEventThrottle={16}
      >
        <View style={{paddingTop: H_MAX_HEIGHT }}>
          {/** Page contant goes here **/}
          {children}
        </View>
      </ScrollView>
      {
        /** 
         * We put the header at the bottom of
         * our JSX or it will not take priority
         * on Android (for some reason, simply
         * setting zIndex does not work)
         **/
      }
      <Animated.View
        style={{
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
          height: headerScrollHeight,
          width: "100%",
          overflow: "hidden",
          zIndex: 999,
          // STYLE
        }}
        >
      <ImageBackground resizeMode="stretch"  style={{width:"100%",height:150}} 
      source={Appearance.getColorScheme()=="dark"?headerbgp:headerbg} >
       <Animated.View style={[styles.head,{height:headerScrollHeight}]}>
          <Animated.Text style={[styles.tag1,{fontSize:ATFONT,bottom:aTotalTop}]}>ACCOUNT TOTAL</Animated.Text>
          <Animated.View style={{flexDirection:"row",marginTop:15,alignItems:"center",position:"absolute",top:textTopMargin,left:textScrolLeft}}>
            <Animated.Text style={[styles.tag2,{fontSize:fontSizeInterpolate}]}>MKI 4,2411</Animated.Text>
            {H_MAX_HEIGHT !=headerScrollHeight &&
            <Text style={[styles.tag3]}>EQUALS 4,3225G GOLD</Text>
            }
          </Animated.View>
        </Animated.View>
        </ImageBackground>
      </Animated.View>
    </View>
  )
}

export default CollapsibleHeader;
const styles=StyleSheet.create({
    head:{
      // backgroundColor:Appearance.getColorScheme()=="dark"?colors.primary:colors.black,
      padding:20
    },
    tag1:{
      fontFamily:"Roboto-Light",
      color:Appearance.getColorScheme()=="dark"?colors.black20:colors.grey,
      fontSize:15,
      marginTop:10
    },
    tag2:{
      fontFamily:"Roboto-Regular",
      // fontSize:30,
      color:Appearance.getColorScheme()=="dark"?colors.black10:colors.white,
    },
    tag3:{
      flex:1,
      textAlign:"right",
      fontFamily:"Roboto-Regular",
      color:Appearance.getColorScheme()=="dark"?colors.black20:colors.grey,
      fontSize:13,
    },
  })