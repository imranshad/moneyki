import React from 'react';
import { Text, View, ImageBackground, StyleSheet, Image, Appearance, TouchableOpacity,StatusBar } from 'react-native';
import { colors } from 'services/Theme';
import { arrow_left, arrow_left_white } from 'assets/icons'
import { headerbg2, headerbgp2, hrline } from 'assets/images';
const Header = ({
    title,
    navigation,
    style,
    isBlack,
    iconStyle,
    RightBtn,
    gradient
}) => {
    if (gradient) {
        return (
            <>
            <ImageBackground resizeMode="stretch" style={{ width: "100%", height: 70, flexDirection: "row", }}
                source={Appearance.getColorScheme() == "dark" ? headerbgp2 : headerbg2} >
                {navigation.canGoBack() &&
                    <TouchableOpacity style={[styles.icon, iconStyle, { marginLeft: 20 }]} onPress={() => navigation.goBack()}>
                        <Image style={styles.icon} source={isBlack ? arrow_left_white : arrow_left} resizeMode="contain" />
                    </TouchableOpacity>
                }
                <Text style={[styles.title, { color: isBlack ? colors.white : colors.black }]}>{title}</Text>
                {RightBtn && <RightBtn />}
            </ImageBackground>
            <View style={{ height: 1, backgroundColor: Appearance.getColorScheme()=="dark"?"#e3c87b":colors.grey}} />
            </>
        )
    }
    return (
        <View style={[styles.container, style]}>
            {navigation.canGoBack() &&
                <TouchableOpacity style={[styles.icon, iconStyle]} onPress={() => navigation.goBack()}>
                    <Image style={styles.icon} source={isBlack ? arrow_left_white : arrow_left} resizeMode="contain" />
                </TouchableOpacity>
            }
            <Text style={[styles.title, { color: isBlack ? colors.white : colors.black }]}>{title}</Text>
            {RightBtn && <RightBtn />}
        </View>
    );
}

export default Header;
const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: 70,
        flexDirection: "row",
        top: 0,
        alignItems: "center",
        paddingLeft: 20,
        paddingRight: 20
    },
    title: {
        fontFamily: "Roboto-Medium",
        fontSize: 16,
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center",
    },
    icon: {
        width: 25,
        height: 25,
        alignSelf: "center",
        position: "absolute"
    }
})