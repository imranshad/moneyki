import gold from './gold.png'
import headerbg from './headerbg.png'
import headerbg2 from './headerbg2.png'
import headerbgp from './headerbgp.png'
import headerbgp2 from './headerbgp2.png'
import hrline from './hrline.png'

export {
    gold,
    headerbg,
    headerbg2,
    headerbgp,
    headerbgp2,
    hrline
}