
export const colors={
    background:"#FFFFFF",
    primary:"#C2A758",
    primaryLight:"#F3EDDD",
    sfotLight:"#F5F6FF",
    grey:"#989898",
    black:"#000000",
    black10:"#1F1F1F",
    black20:"#505050",
    white:"#FFFFFF",
    cgreen:"#57EFD0",
    lightRed:"#FF8787",
    border:"#989898"
}