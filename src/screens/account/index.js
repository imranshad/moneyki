import React, { Component } from 'react';
import { View, StyleSheet, Appearance, TouchableOpacity, Image } from 'react-native';
import {Header2 as Header, ListHead,ListItem,Colapsible } from 'components'
import { colors } from 'services/Theme';
import { filter } from 'assets/icons';

export default class index extends Component {
  FilterBtn=()=>
  <TouchableOpacity 
  style={{top:20,right:20,position:"absolute"}}>
    <Image source={filter} resizeMode="contain" />
  </TouchableOpacity>

  render() {
    const {navigation}=this.props
    return (
      <View style={{flex:1}}>
        <Header
        gradient
        iconStyle={{marginLeft:20}}
        isBlack
        navigation={navigation} 
        style={styles.header}
        RightBtn={()=><this.FilterBtn />}
        title="Transaction list" />
      <View style={styles.container}>
        <Colapsible>
        <>
        <ListHead date="05.05.2020" balance="4,2411" />
          <ListItem title="FD@HPM.AGENCY" fee="0,0002" time="12:32h" mki="-0,0254" arrowRed />
          <View style={{height:0.8,backgroundColor:colors.grey,opacity:0.2}} />
          <ListItem title="FD@HPM.AGENCY" fee="0,0002" time="12:32h" mki="-0,0254" arrowRed/>
          <ListHead date="05.05.2020" balance="4,2411" />
          <View style={{height:0.8,backgroundColor:colors.grey,opacity:0.2}} />
          <ListItem title="FD@HPM.AGENCY" fee="0,0002" time="12:32h" mki="-0,0254"  />
          <View style={{height:0.8,backgroundColor:colors.grey,opacity:0.2}} />
          <ListItem title="FD@HPM.AGENCY" fee="0,0002" time="12:32h" mki="-0,0254" arrowRed/>
          <View style={{height:0.8,backgroundColor:colors.grey,opacity:0.2}} />
          <ListItem title="FD@HPM.AGENCY" fee="0,0002" time="12:32h" mki="-0,0254" />
          <View style={{height:0.8,backgroundColor:colors.grey,opacity:0.2}} />
          <ListItem title="FD@HPM.AGENCY" fee="0,0002" time="12:32h" mki="-0,0254"  />
          <View style={{height:0.8,backgroundColor:colors.grey,opacity:0.2}} />
          <ListItem title="FD@HPM.AGENCY" fee="0,0002" time="12:32h" mki="-0,0254" arrowRed/>
          <View style={{height:0.8,backgroundColor:colors.grey,opacity:0.2}} />
          <ListItem title="FD@HPM.AGENCY" fee="0,0002" time="12:32h" mki="-0,0254" />
        </>
        </Colapsible>
      </View>
      </View>
    );
  }
}
const styles=StyleSheet.create({
  container:{
    backgroundColor:Appearance.getColorScheme()=="dark"?colors.black:colors.background,
    flex:1
  },
  header:{
    backgroundColor:Appearance.getColorScheme()=="dark"?colors.primary:colors.black,
    elevation:2
  },
  head:{
    backgroundColor:Appearance.getColorScheme()=="dark"?colors.primary:colors.black,
    padding:40
  },
  tag1:{
    fontFamily:"Roboto-Light",
    color:colors.sfotLight
  },
  tag2:{
    fontFamily:"Roboto-Regular",
    fontSize:22,
    color:colors.white
  },
  tag3:{
    flex:1,
    textAlign:"right",
    fontFamily:"Roboto-Regular",
    color:colors.sfotLight
  }
})