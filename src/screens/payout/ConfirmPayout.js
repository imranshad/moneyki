import React, { Component } from 'react';
import { View, Image, TextInput, ScrollView, Text, TouchableOpacity, Appearance,StyleSheet } from 'react-native';
import { user, arrwo_up_right, transfer_fee, edit, confirm_payout } from 'assets/icons'
import { stylesDark, stylesNormal } from './Style'
import { Header, Button } from 'components'
import { colors } from 'services/Theme';

export default class ConfirmPayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MKI: "0,000",
      euro: "0,000",
      styles: Appearance.getColorScheme() == "dark" ? stylesDark : stylesNormal
    };
  }

  render() {
    const { styles } = this.state
    return (
      <View style={{ flex: 1,backgroundColor:Appearance.getColorScheme()=="dark"?colors.black:colors.background }}>
        <Header iconStyle={{ marginLeft: 20 }} title="Confirm Pay Out" navigation={this.props.navigation} />
        <ScrollView>
          <View style={styles.container}>
            <Image source={confirm_payout} resizeMode="contain" style={{ marginTop: "15%", width: 80, height: 80, alignSelf: "center" }} />
            <Text style={styles.lable1}>Receiving IBAN </Text>
            <View style={styles.inpCont}>
              <TextInput
                style={styles.inpt}
                value="DE12 3456 7890 1234 56"
                placeholder="user@mail.com" />
            </View>
            <Text style={styles.lable1}>Amount</Text>

            <View style={styles.inpCont2}>
              <View style={styles2.inp2Cont}>
                <TextInput
                  maxLength={10}
                  keyboardType="numeric"
                  onChangeText={(MKI) => this.setState({ MKI: numberWithoutCommas(MKI) })}
                  style={{ textAlign: "right", fontSize: 20, fontFamily: "Roboto-Regular", color: colors.primary, paddingLeft: 10 }}
                  value={numberWithCommas(this.state.MKI)} />
                <Text style={{ color: colors.grey, fontSize: 10, textAlign: "left", alignSelf: "flex-end", paddingRight: 5, marginBottom: 18 }}> g</Text>
              </View>
              <Text style={styles.eqSymb}>=</Text>
              <View style={styles2.inp2Cont}>
                <Text style={{ textAlign: "right", alignSelf: "center", fontSize: 20, fontFamily: "Roboto-Regular", color: colors.primary, paddingLeft: 10 }}>€</Text>
                <TextInput
                  maxLength={10}
                  keyboardType="numeric"
                  onChangeText={(euro) => this.setState({ euro: numberWithoutCommas(euro) })}
                  style={styles2.inp2}
                  value={numberWithCommas(this.state.euro)} />
                <Text style={{ color: colors.grey, fontSize: 10, textAlign: "left", alignSelf: "flex-end", paddingRight: 5, marginBottom: 18 }}> Roughly</Text>
              </View>
            </View>

            <View style={[styles.inp2, { width: "100%", marginTop: 10, flexDirection: "row", justifyContent: "space-between" }]}>
              <View style={{ flexDirection: "row" }}>
                <Image source={transfer_fee} resizeMode="contain" />
                <Text style={styles.transferFeeTxt}>Transfer Fee:</Text>
              </View>
              <Text style={styles.transferFeeTxt}>0,0002 MKI</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 30, marginBottom: 30 }}>
              <Button
                onPress={() => this.props.navigation.goBack()}
                style={{ flex: 1, marginRight: 10, flexDirection: "row" }}>
                <Image source={edit} resizeMode="contain" style={{ marginRight: 10 }} />
                <Text style={{ fontFamily: "Roboto-Medium", color: colors.grey, fontSize: 16 }}>EDIT</Text>
              </Button>
              <Button
                onPress={() => this.props.navigation.navigate("Account")}
                style={{ flex: 1, marginLeft: 10, borderColor: colors.primary }}>
                <Text style={{ color: colors.primary, fontFamily: "Roboto-Medium", fontSize: 16 }}>CANCEL</Text>
              </Button>
            </View>
            <View style={{ flex: 1, justifyContent: "center" }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Account")}
                style={styles.btn}>
                <Image resizeMode="contain" style={styles.ico2} source={arrwo_up_right} />
                <Text style={styles.sendTxt}>SEND</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
function numberWithCommas(x) {
  if (!x) return null
  return String(x).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function numberWithoutCommas(a) {
  if (!a) return null
  a = String(a).replace(/\,/g, ''); // 1125, but a string, so convert it to number
  a = parseInt(a, 10);
  return a
}
const styles2=StyleSheet.create({
  inp2Cont:{ 
    height: 55, 
    backgroundColor: Appearance.getColorScheme()=="dark"?colors.black10:colors.sfotLight, 
    borderRadius: 5, flex: 1,
    flexDirection: "row", justifyContent: "center"
  },
  inp2:{ textAlign: "right", fontSize: 20, fontFamily: "Roboto-Regular", color: colors.primary, paddingLeft: 10 }
})