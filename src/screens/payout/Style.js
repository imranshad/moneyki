import {StyleSheet} from 'react-native'
import { colors } from 'services/Theme';

export const stylesNormal = StyleSheet.create({
    container: {
      backgroundColor: colors.background,
      flex: 1,
      padding: 20
    },
    lable1:{
      fontFamily:"Roboto-Medium",
      color:colors.black,
      marginTop:40,
      marginBottom:15,
      fontSize:16
    },
    inpt: {
      fontFamily:"Roboto-Medium",
      backgroundColor:colors.sfotLight,
      fontSize:15,
      color:colors.black,
      padding:15
    },
    ico:{
      margin:10,
      width:25,
      height:25,
      marginRight:20
    },
    inpCont:{
      flexDirection:"row",
      paddingTop: 10,
      paddingBottom: 10,
      borderRadius: 5,
      backgroundColor: colors.sfotLight
    },
    inpCont2:{
      flexDirection:"row",
    },
    inp2:{
      fontFamily:"Roboto-Regular",
      color:colors.primary,
      fontSize:22,
      backgroundColor:colors.sfotLight,
      padding:20,
      borderRadius:5,
      flex:1
    },
    eqSymb:{
      textAlign:"center",
      alignSelf:"center",
      marginLeft:"5%",
      marginRight:"5%",
      alignItems:"center",
      justifyContent:"center",
      fontFamily:"Roboto-Regular"
    },
    btn:{
      width:"100%",
      borderRadius:5,
      backgroundColor:colors.primary,
      flexDirection:"row",
      alignItems:"center",
      justifyContent:"center",
      padding:5
    },
    ico2:{
      width:15,
      height:15,
      alignSelf:"center",
      margin:15
    },
    sendTxt:{
      color:colors.white,
      fontFamily:"Roboto-Medium",
      fontSize:16,
    },
    transferFeeTxt:{
      fontSize:15,
      marginLeft:10,
      fontFamily:"Roboto-Regular",
      textAlign:"right",
      color:colors.black
    }
  })
//   dark mode
 export const stylesDark = StyleSheet.create({
    container: {
      backgroundColor: colors.black,
      flex: 1,
      padding: 20
    },
    lable1:{
      fontFamily:"Roboto-Medium",
      color:colors.white,
      marginTop:40,
      marginBottom:15,
      fontSize:16
    },
    inpt: {
      fontFamily:"Roboto-Medium",
      color:colors.white,
      backgroundColor:colors.black10,
      padding:15
    },
    ico:{
      margin:10,
      width:25,
      height:25,
      marginRight:20
    },
    inpCont:{
      flexDirection:"row",
      paddingTop: 10,
      paddingBottom: 10,
      borderRadius: 5,
      backgroundColor: colors.black10
    },
    inpCont2:{
      flexDirection:"row",
    },
    inp2:{
      fontFamily:"Roboto-Regular",
      color:colors.primary,
      fontSize:22,
      backgroundColor:colors.black10,
      padding:20,
      borderRadius:5,
      flex:1
    },
    eqSymb:{
      textAlign:"center",
      alignSelf:"center",
      marginLeft:"5%",
      marginRight:"5%",
      color:colors.primary,
      fontFamily:"Roboto-Regular"
    },
    btn:{
      width:"100%",
      borderRadius:5,
      backgroundColor:colors.primary,
      flexDirection:"row",
      alignItems:"center",
      justifyContent:"center",
      padding:5
    },
    ico2:{
      width:15,
      height:15,
      margin:15
    },
    sendTxt:{
      color:colors.white,
      fontFamily:"Roboto-Medium",
      fontSize:16,
    },
    transferFeeTxt:{
      marginLeft:10,
      fontSize:15,
      fontFamily:"Roboto-Regular",
      textAlign:"right",
      color:colors.white
    }
  })