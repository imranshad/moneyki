import React, { Component } from 'react';
import { View, Text, StyleSheet, Appearance, TouchableOpacity, Image, ScrollView } from 'react-native';
import { Header2 as Header, Colapsible } from 'components'
import { colors } from 'services/Theme';
import { transfer_fee, arrwo_up_right } from 'assets/icons';
import { TextInput } from 'react-native-gesture-handler';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MKI: "0,000",
      euro: "0,000",
    }
  }

  render() {
    const { navigation } = this.props
    return (
      <View style={{ flex: 1 }}>
        <Header
        gradient
          isBlack
          navigation={navigation}
          style={styles.header}
          title="Pay Out" />
        <View style={styles.container}>
          <Colapsible>
            <View style={{ flex: 1, padding: 20 }}>
              <Text style={styles.lable1}>Please place your selling order:</Text>
              <Text style={styles.lable1}>Receiving IBAN</Text>
              {/* Form */}
              <TextInput
                style={styles.inpt} />

              <Text style={styles.lable1}>Amount</Text>

              <View style={{flexDirection:"row"}}>
              <View style={styles.inp2Cont}>
                <TextInput
                  maxLength={10}
                  keyboardType="numeric"
                  onChangeText={(MKI) => this.setState({ MKI: numberWithoutCommas(MKI) })}
                  style={styles.inp3}
                  value={numberWithCommas(this.state.MKI)} />
                <Text style={{ color: colors.grey, fontSize: 10, textAlign: "left", alignSelf: "flex-end", paddingRight: 5, marginBottom: 18 }}> MKI</Text>
              </View>
              <Text style={styles.eqSymb}>=</Text>
              <View style={styles.inp2Cont}>
                <Text style={{ textAlign: "right", alignSelf: "center", fontSize: 20, fontFamily: "Roboto-Regular", color: colors.primary, paddingLeft: 10 }}>€</Text>
                <TextInput
                  maxLength={10}
                  keyboardType="numeric"
                  onChangeText={(euro) => this.setState({ euro: numberWithoutCommas(euro) })}
                  style={styles.inp3}
                  value={numberWithCommas(this.state.euro)} />
                <Text style={{ color: colors.grey, fontSize: 10, textAlign: "left", alignSelf: "flex-end", paddingRight: 5, marginBottom: 18 }}> Roughly</Text>
              </View>
            </View>

              <View style={[styles.inp2, { width: "100%", marginTop: 10, flexDirection: "row", justifyContent:"space-between" }]}>
                <View style={{flexDirection:"row"}}>
                <Image source={transfer_fee} resizeMode="contain" />
                <Text style={styles.transferFeeTxt}>Transfer Fee:</Text>
                </View>
                <Text style={styles.transferFeeTxt}>0,0002 MKI</Text>
              </View>
            </View>
            <View style={{flex:1,marginLeft:20,marginRight:20}}>
                <TouchableOpacity
                  onPress={() => navigation.navigate("ConfirmPayout")}
                  style={styles.btn}>
                  <Image resizeMode="contain" style={styles.ico2} source={arrwo_up_right} />
                  <Text style={styles.btnTxt}>PAY OUT</Text>
                </TouchableOpacity>
              </View>
          </Colapsible>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: Appearance.getColorScheme() == "dark" ? colors.black : colors.background,
    flex: 1
  },
  header: {
    backgroundColor: Appearance.getColorScheme() == "dark" ? colors.primary : colors.black,
    elevation: 5,
    borderWidth: 0.4,
    borderBottomColor: "#505050"
  },
  head: {
    backgroundColor: Appearance.getColorScheme() == "dark" ? colors.primary : colors.black,
    padding: 40
  },
  tag1: {
    fontFamily: "Roboto-Light",
    color: colors.sfotLight
  },
  tag2: {
    fontFamily: "Roboto-Regular",
    fontSize: 22,
    color: colors.white
  },
  tag3: {
    flex: 1,
    textAlign: "right",
    fontFamily: "Roboto-Regular",
    color: colors.sfotLight
  }, inpt: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    padding: 10,
    borderRadius: 5,
    height: 60,
    backgroundColor: Appearance.getColorScheme() == "dark" ? colors.black10 : colors.sfotLight
  },
  lable1: {
    fontFamily: "Roboto-Medium",
    color: Appearance.getColorScheme() == "dark" ? colors.white : colors.black,
    marginTop: 40,
    marginBottom: 15,
    fontSize: 16
  },
  tag5: {
    fontFamily: "Roboto-Regular",
    color: colors.primary,
    fontSize: 22,
    backgroundColor: Appearance.getColorScheme() == "dark" ? colors.black10 : colors.sfotLight,
    padding: 25,
    width: "45%"
  },
  inp2Cont:{ 
    height: 55, 
    backgroundColor: Appearance.getColorScheme()=="dark"?colors.black10:colors.sfotLight, 
    borderRadius: 5, flex: 1,
    flexDirection: "row", justifyContent: "center"
  },
  inp3:{ textAlign: "right", fontSize: 20, fontFamily: "Roboto-Regular", color: colors.primary, paddingLeft: 10 },
  eqSymb: {
    textAlign: "center",
    alignSelf: "center",
    marginLeft: "5%",
    marginRight: "5%",
    color: colors.primary,
    fontFamily: "Roboto-Regular"
  },
  transferFeeTxt: {
    fontFamily: "Roboto-Regular",
    fontSize:15,
    marginLeft:10,
    color: Appearance.getColorScheme() == "dark" ? colors.white : colors.black
  },
  btn: {
    width: "100%",
    borderRadius: 5,
    marginTop: 20,
    backgroundColor: colors.primary,
    flexDirection: "row",
    alignSelf:"center",
    alignItems: "center",
    justifyContent: "center",
  },
  ico2: {
    width: 15,
    height: 15,
    margin: 15
  },
  inp2: {
    fontFamily: "Roboto-Regular",
    color: colors.primary,
    fontSize: 20,
    backgroundColor: Appearance.getColorScheme() == "dark" ? colors.black10 : colors.sfotLight,
    padding: 20,
    borderRadius:5,
    flex:1
  },
  btnTxt: {
    color: colors.white,
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    paddingTop: 20,
    paddingBottom: 20
  },
})
function numberWithCommas(x) {
  if (!x) return null
  return String(x).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function numberWithoutCommas(a) {
  if (!a) return null
  a = String(a).replace(/\,/g, ''); // 1125, but a string, so convert it to number
  a = parseInt(a, 10);
  return a
}