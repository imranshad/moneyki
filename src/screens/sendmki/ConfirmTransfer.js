import React, { Component } from 'react';
import { View, Image, TextInput, Text, TouchableOpacity, ScrollView, Appearance,StyleSheet } from 'react-native';
import { send, user, arrwo_up_right, confirm_ico, transfer_fee, edit } from 'assets/icons'
import { Header, Button } from 'components'
import { colors } from 'services/Theme';

export default class SendMKI extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MKI: "0000",
      euro: "0000",
      inpText: "FD@HPM.AGENCY",
    };
  }
  getBtnColor() {
    const { MKI, euro } = this.state
    if (MKI && euro && MKI > 0 && euro > 0) {
      return colors.primary
    }
    return colors.grey
  }
  render() {
    const { navigate } = this.props.navigation
    return (
      <View style={{ flex: 1 }}>
        <Header iconStyle={{ marginLeft: 20 }} title="Confirm Transfer" navigation={this.props.navigation} />
        <ScrollView>
          <View style={styles.container}>
            <Image source={confirm_ico} resizeMode="contain" style={{ marginTop: "15%", width: 110, height: 110, alignSelf: "center" }} />
            <Text style={styles.lable1}>Send MKI to </Text>
            <View style={styles.inpCont}>
              <Image source={user} style={styles.ico} resizeMode="contain" />
              <TextInput
                onChangeText={(inpText) => this.setState({ inpText })}
                placeholderTextColor={colors.grey}
                style={styles.inpt}
                value={this.state.inpText}
                placeholder="user@mail.com" />
            </View>
            <Text style={styles.lable1}>Amount</Text>
            <View style={styles.inpCont2}>
              <View style={styles2.inp2Cont}>
                <TextInput
                  maxLength={10}
                  keyboardType="numeric"
                  onChangeText={(MKI) => this.setState({ MKI: numberWithoutCommas(MKI) })}
                  style={styles2.inp2}
                  value={numberWithCommas(this.state.MKI)} />
                <Text style={{ color: colors.grey, fontSize: 10, textAlign: "left", alignSelf: "flex-end", paddingRight: 5, marginBottom: 18 }}> MKI</Text>
              </View>
              <Text style={styles.eqSymb}>=</Text>
              <View style={styles2.inp2Cont}>
                <Text style={{ textAlign: "right", alignSelf: "center", fontSize: 20, fontFamily: "Roboto-Regular", color: colors.primary, paddingLeft: 10 }}>€</Text>
                <TextInput
                  maxLength={10}
                  keyboardType="numeric"
                  onChangeText={(euro) => this.setState({ euro: numberWithoutCommas(euro) })}
                  style={styles2.inp2}
                  value={numberWithCommas(this.state.euro)} />
                <Text style={{ color: colors.grey, fontSize: 10, textAlign: "left", alignSelf: "flex-end", paddingRight: 5, marginBottom: 18 }}> Roughly</Text>
              </View>
            </View>
            <View style={[styles.inp2, { width: "100%", marginTop: 30, flexDirection: "row", alignItems: "center" }]}>
              <View style={{ flexDirection: "row", flex: 1, alignItems: "center", marginLeft: 20 }}>
                <Image source={transfer_fee} resizeMode="contain" />
                <Text style={styles.transferFeeTxt}>Transfer Fee:</Text>
              </View>
              <Text style={[styles.transferFeeTxt, { textAlign: "right", marginRight: 20 }]}>0,0002 MKI</Text>
            </View>
            <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 20, marginBottom: 20 }}>
              <Button
                onPress={() => this.props.navigation.goBack()}
                style={{ flex: 1, flexDirection: "row", marginRight: 10 }}>
                <Image source={edit} resizeMode="contain" style={{ margin: 10 }} />
                <Text style={styles.editTxt}>EDIT</Text>
              </Button>
              <Button
                onPress={() => this.props.navigation.navigate("Account")}
                style={{ flex: 1, marginLeft: 10, borderColor: colors.primary }}>
                <Text style={{ color: colors.primary, fontFamily: "Roboto-Medium", fontSize: 16 }}>CANCEL</Text>
              </Button>
            </View>
            <View style={{ flex: 1, justifyContent: "center" }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Account")}
                style={{
                  width: "100%",
                  borderRadius: 5,
                  backgroundColor: this.getBtnColor(),
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Image resizeMode="contain" style={styles.ico2} source={arrwo_up_right} />
                <Text style={styles.sendTxt}>SEND</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
function numberWithCommas(x) {
  if (!x) return null
  return String(x).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function numberWithoutCommas(a) {
  if (!a) return null
  a = String(a).replace(/\,/g, ''); // 1125, but a string, so convert it to number
  a = parseInt(a, 10);
  return a
}
const styles2=StyleSheet.create({
  inp2Cont:{ 
    height: 55, 
    backgroundColor: Appearance.getColorScheme()=="dark"?colors.black10:colors.sfotLight, 
    borderRadius: 5, flex: 1,
    flexDirection: "row", justifyContent: "center"
  },
  inp2:{ textAlign: "right", fontSize: 20, fontFamily: "Roboto-Regular", color: colors.primary, paddingLeft: 10 }
})

export const styles = StyleSheet.create({
  container: {
    backgroundColor: Appearance.getColorScheme()=="dark"?colors.black:colors.background,
    flex: 1,
    padding: 20
  },
  lable1:{
    fontFamily:"Roboto-Medium",
    color:Appearance.getColorScheme()=="dark"?colors.white:colors.black,
    marginTop:40,
    marginBottom:15,
    fontSize:16
  },
  lable2:{
    fontFamily:"Roboto-Regular",
    color:Appearance.getColorScheme()=="dark"?colors.white:colors.black,
    marginTop:40,
    marginBottom:15,
    fontSize:16
  },
  inpt: {
    fontFamily:"Roboto-Regular",
    color:Appearance.getColorScheme()=="dark"?colors.white:colors.black,
    fontSize:15,
    backgroundColor:Appearance.getColorScheme()=="dark"?colors.black10:colors.sfotLight
  },
  ico:{
    margin:10,
    width:25,
    height:25,
    marginRight:20
  },
  inpCont:{
    flexDirection:"row",
    paddingTop: 10,
    paddingBottom: 10,
    borderRadius: 5,
    backgroundColor:Appearance.getColorScheme()=="dark"?colors.black10:colors.sfotLight
  },
  inpCont2:{
    flexDirection:"row",
  },
  inp2:{
    fontFamily:"Roboto-Regular",
    color:colors.primary,
    fontSize:20,
    backgroundColor:Appearance.getColorScheme()=="dark"?colors.black10:colors.sfotLight,
    textAlign:"center",
    textAlignVertical:"center",
    height:55,
    borderRadius:5,
    flex:1,
  },
  eqSymb:{
    textAlign:"center",
    alignSelf:"center",
    marginLeft:"2%",
    marginRight:"2%",
    fontSize:16,
    fontFamily:"Roboto-Regular"
  },
  btn:{
    width:"100%",
    borderRadius:5,
    backgroundColor:colors.grey,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center",
  },
  ico2:{
    width:15,
    height:15,
    margin:10
  },
  sendTxt:{
    color:colors.white,
    fontFamily:"Roboto-Medium",
    fontSize:16,
    paddingTop:20,
    paddingBottom:20
  },
  transferFeeTxt:{
    flex:1,
    fontSize:15,
    fontFamily:"Roboto-Regular",
    color:Appearance.getColorScheme()=="dark"?colors.white:colors.black
  },
  editTxt:{
    color:colors.grey,
    fontFamily: "Roboto-Medium",
    fontSize:16, 
  }
})