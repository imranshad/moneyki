import {StyleSheet} from 'react-native'
import { colors } from 'services/Theme';

export const stylesNormal = StyleSheet.create({
    container: {
      backgroundColor: colors.background,
      flex: 1,
      padding: 20
    },
    lable1:{
      fontFamily:"Roboto-Medium",
      color:colors.black,
      marginTop:40,
      marginBottom:15,
      fontSize:16
    },
    lable2:{
      fontFamily:"Roboto-Regular",
      color:colors.black,
      marginTop:40,
      marginBottom:15,
      fontSize:16
    },
    inpt: {
      fontFamily:"Roboto-Regular",
      color:colors.black,
      fontSize:15,
      backgroundColor:colors.sfotLight
    },
    ico:{
      margin:10,
      width:25,
      height:25,
      marginRight:20
    },
    inpCont:{
      flexDirection:"row",
      paddingTop: 10,
      paddingBottom: 10,
      borderRadius: 5,
      backgroundColor: colors.sfotLight
    },
    inpCont2:{
      flexDirection:"row",
    },
    inp2:{
      fontFamily:"Roboto-Regular",
      color:colors.primary,
      fontSize:20,
      backgroundColor:colors.sfotLight,
      textAlign:"center",
      textAlignVertical:"center",
      height:55,
      borderRadius:5,
      flex:1,
    },
    eqSymb:{
      textAlign:"center",
      alignSelf:"center",
      marginLeft:"2%",
      marginRight:"2%",
      fontSize:16,
      fontFamily:"Roboto-Regular"
    },
    btn:{
      width:"100%",
      borderRadius:5,
      backgroundColor:colors.grey,
      flexDirection:"row",
      alignItems:"center",
      justifyContent:"center",
    },
    ico2:{
      width:15,
      height:15,
      margin:10
    },
    sendTxt:{
      color:colors.white,
      fontFamily:"Roboto-Medium",
      fontSize:16,
      paddingTop:20,
      paddingBottom:20
    },
    transferFeeTxt:{
      flex:1,
      fontSize:15,
      fontFamily:"Roboto-Regular",
      color:colors.black
    },
    editTxt:{
      color:colors.grey,
      fontFamily: "Roboto-Medium",
      fontSize:16, 
    }
  })
//   dark mode
 export const stylesDark = StyleSheet.create({
    container: {
      backgroundColor: colors.black,
      flex: 1,
      padding: 20
    },
    lable1:{
      fontFamily:"Roboto-Medium",
      color:colors.white,
      marginTop:40,
      marginBottom:15,
      fontSize:16
    },
    lable2:{
      fontFamily:"Roboto-Regular",
      color:colors.white,
      marginTop:40,
      marginBottom:15,
      fontSize:16
    },
    inpt: {
      fontFamily:"Roboto-Regular",
      fontSize:15,
      color:colors.white,
      backgroundColor:colors.black10
    },
    ico:{
      margin:10,
      width:25,
      height:25,
      marginRight:20
    },
    inpCont:{
      flexDirection:"row",
      paddingTop: 10,
      paddingBottom: 10,
      borderRadius: 5,
      backgroundColor: colors.black10
    },
    inpCont2:{
      flexDirection:"row",
    },
    inp2:{
      fontFamily:"Roboto-Regular",
      color:colors.primary,
      fontSize:20,
      backgroundColor:colors.black10,
      textAlign:"center",
      textAlignVertical:"center",
      height:55,
      borderRadius:5,
      flex:1,
      flexGrow:1
    },
    eqSymb:{
      textAlign:"center",
      alignSelf:"center",
      marginLeft:"2%",
      marginRight:"2%",
      color:colors.primary,
      fontFamily:"Roboto-Regular"
    },
    btn:{
      width:"100%",
      borderRadius:5,
      backgroundColor:colors.primary,
      flexDirection:"row",
      alignItems:"center",
      justifyContent:"center",
    },
    ico2:{
      width:15,
      height:15,
      margin:10
    },
    sendTxt:{
      color:colors.white,
      fontFamily:"Roboto-Medium",
      fontSize:16,
      paddingTop:20,
      paddingBottom:20
    },
    transferFeeTxt:{
      flex:1,
      fontSize:15,
      fontFamily:"Roboto-Regular",
      marginLeft:10,
      color:colors.white
    },
    editTxt:{
      color:colors.grey,
      fontFamily: "Roboto-Medium",
      fontSize:16, 
    }
  })