import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Appearance } from 'react-native';
import { Header, Button } from 'components'
import { gold } from 'assets/images'
import { colors } from 'services/Theme';
import { check_square, send_outline_focused, hash, cart } from 'assets/icons';
import { ScrollView } from 'react-native-gesture-handler';

export default class ConfirmPayin extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { navigation } = this.props
    return (
      <View style={{ flex: 1 }}>
        <Header iconStyle={{ marginLeft: 20 }} navigation={navigation} title="Confirm pay in" />
        <ScrollView>
          <View style={styles.container}>
            <View>
              <Image source={gold} resizeMode="stretch" style={styles.img} />
              <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 15 }}>
                <Text style={styles.tag1}>Unallocated share in weight </Text>
                <View style={{alignSelf:"flex-end"}}>
                  <Text style={styles.tag2}>EUR 52,30/g </Text>
                  <Text style={styles.tag3}>100,0000 g</Text>
                </View>
              </View>
              <View style={styles.totalCont}>
                <Text style={styles.tag4}>TOTAL</Text>
                <Text style={styles.tag5}>EUR 523,00</Text>
              </View>
              <View style={styles.para1}>
                <Image style={{ marginRight: 10, marginTop: 5 }} source={check_square} resizeMode="contain" />
                <Text style={styles.tag6}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been</Text>
              </View>
              <View style={{ height: 0.8, backgroundColor: colors.grey, opacity: 0.2, marginTop: 20, marginBottom: 15 }} />
              <View style={styles.para1}>
                <Image style={{ marginRight: 10 }} source={send_outline_focused} resizeMode="contain" />
                <Text style={[styles.tag6, { fontFamily: "Roboto-Regular" }]}>To complete your Pay In, please transfer EUR 523,00 to</Text>
              </View>
              <View style={{marginTop:30,marginLeft:30}}>
                <Text style={styles.tag8}>Account Holder</Text>
                <Text style={styles.tag9}>Moneki GmbH</Text>
                <Text style={styles.tag10}>IBAN</Text>
                <Text style={styles.tag11}>DE12 3456 7890 1234 56</Text>
                <Text style={styles.tag12}>Send MKI to</Text>
                <View style={{ flexDirection: "row" }}>
                  <Image source={hash} resizeMode="contain" />
                  <Text style={styles.tag13}>QBYK2</Text>
                </View>
              </View>
              <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 20 }}>
                <Button 
                onPress={()=>navigation.goBack()}
                style={[styles.btn]}>
                  <Text style={{ color: colors.grey, fontFamily: "Roboto-Medium", fontSize: 16 }}>CANCEL</Text>
                </Button>
                <Button
                  onPress={() => navigation.navigate("Account")}
                  style={[styles.btn, { backgroundColor: colors.primary, borderWidth: 0, flexDirection: "row" }]}>
                  <Image source={cart} resizeMode="contain" />
                  <Text style={styles.buyNow}>BUY NOW</Text>
                </Button>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Appearance.getColorScheme() == "dark" ? colors.black : colors.background,
    padding: 18
  },
  img: {
    width: "100%",
  },
  tag1: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: Appearance.getColorScheme() == "dark" ? colors.white : colors.black
  },
  tag2: {
    color: colors.primary,
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    textAlign: "right"
  },
  totalCont: {
    flexDirection: "row", borderTopWidth: 1, marginTop: 5,
    justifyContent: "space-between", borderColor: colors.primary,
    paddingTop: 10
  },
  tag3: {
    color: colors.primary,
    fontFamily: "Roboto-Medium",
    fontSize: 14,
    textAlign: "right",
    marginTop: 8,
  },
  tag4: {
    color: colors.primary,
    fontFamily: "Roboto-Medium",
    fontSize: 16,
  },
  tag5: {
    color: colors.primary,
    fontFamily: "Roboto-Medium",
    fontSize: 14,
  },
  tag6: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    marginRight: 20,
    lineHeight:25,
    color: Appearance.getColorScheme() == "dark" ? colors.white : colors.black
  },
  para1: {
    flexDirection: "row", marginTop: 10,
  },
  tag8: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: Appearance.getColorScheme() == "dark" ? colors.white : colors.black,
    marginTop: 5,
    marginBottom: 15
  },
  tag9: {
    fontFamily: "Roboto-Regular",
    color: colors.grey,
    marginBottom: 15
  },
  tag10: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: Appearance.getColorScheme() == "dark" ? colors.white : colors.black,
    marginTop: 10,
    marginBottom: 15
  },
  tag11: {
    fontFamily: "Roboto-Regular",
    color: colors.grey,
    marginBottom: 15
  },
  tag12: {
    fontFamily: "Roboto-Medium",
    fontSize: 16,
    color: Appearance.getColorScheme() == "dark" ? colors.white : colors.black,
    marginTop: 10,
    marginBottom: 15
  },
  tag13: {
    fontFamily: "Roboto-Regular",
    color: colors.grey,
    marginBottom: 15
  },
  btn: {
    width: "45%",
  },
  buyNow:{ color: colors.white, fontFamily: "Roboto-Medium", fontSize: 16, marginLeft: 5 }
})