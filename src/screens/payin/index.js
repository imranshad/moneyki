import React, { Component } from 'react';
import { View, Text, StyleSheet, Appearance, Image,ScrollView } from 'react-native';
import {Header,PayinItem, Button} from 'components'
import { colors } from 'services/Theme';
import { cart } from 'assets/icons';

export default class index extends Component {

  render() {
    const {navigation}=this.props
    return (
      <View style={styles.container}>
        <Header iconStyle={{marginLeft:20}} navigation={navigation} title="Pay In" />
        <ScrollView>
        <Text style={styles.tag1}>
          Please choose a product or select an {'\n'}
          unallocated share in weight from our {'\n'}
          collective deposit {'\n'}
        </Text>
        <View>
          <PayinItem title="0.5 oz Gold Bar"
          desc="Contain 0.5 oz of .9999 fine Gold, best n best purity, from the world's leading"
          price="1.645,00"
          weight="31.1"
          />
          <View style={{height:0.8,backgroundColor:colors.grey,opacity:0.2}} />
          <PayinItem title="0.5 oz Gold Bar"
          desc="Contain 0.5 oz of .9999 fine Gold, best n best purity, from the world's leading"
          price="845,00"
          weight="15.55"
          />
          <View style={{height:0.8,backgroundColor:colors.grey,opacity:0.2}} />
          <PayinItem title="0.5 oz Gold Bar"
          desc="Contain 0.5 oz of .9999 fine Gold, best n best purity, from the world's leading"
          price="1.645,00"
          weight="31.1"
          />
          <PayinItem 
          unallowcated
          title="Unallocated share in weight"
          desc="Contain an unallocated share of                                     "
          price="52,30/g"
          weight="31.1"
          />
          <View style={{height:1,marginLeft:20,marginRight:20,backgroundColor:colors.primary}} />
          <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",padding:20}}>
            <View style={{alignItems:"center",flexDirection:"row"}}>
            <Text style={styles.tag2}>€</Text>
            <Text style={styles.tag3}>TOTAL</Text>
            </View>
            <Text style={styles.tag4}>EUR 1.645,00</Text>
          </View>
          <Button
          onPress={()=>navigation.navigate("ConfirmPayin")}
          style={styles.btn}>
            <Image source={cart} resizeMode="contain" />
            <Text style={styles.btnTxt}>BUY NOW</Text>
          </Button>
        </View>
        </ScrollView>
      </View>
    );
  }
}
const styles=StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:Appearance.getColorScheme()=="dark"?colors.black:colors.white
  },
  tag1:{
    fontFamily:"Roboto-Regular",
    fontSize:15,
    color:Appearance.getColorScheme()=="dark"?colors.white:colors.black,
    margin:20
  },
  tag2:{
    fontFamily:"Roboto-Regular",
    fontSize:20,
    color:colors.primary,
    backgroundColor:Appearance.getColorScheme()=="dark"?colors.black10: colors.sfotLight,
    padding:10,
    paddingLeft:18,
    paddingRight:18,
    borderRadius:5
  },
  tag3:{
    fontFamily:"Roboto-Regular",
    fontSize:23,
    marginLeft:5,
    color:colors.primary,
  },
  tag4:{
    fontFamily:"Roboto-Regular",
    fontSize:23,
    color:colors.primary,
    textAlign:"right"
  },
  btn:{
    backgroundColor:colors.primary,
    borderWidth:0,
    marginLeft:20,
    marginRight:20,
    marginBottom:20,
    flexDirection:"row"
  },
  btnTxt:{
    fontFamily:"Roboto-Medium",
    color:colors.white,
    fontSize:16,
    margin:5
  }
  
})