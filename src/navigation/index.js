import 'react-native-gesture-handler';
import * as React from 'react';
const navigationRef = React.createRef();
function Navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}

import { Appearance,Image,Text,StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {
  arrwo_down_circle,arrwo_down_circle_focused,
  arrwo_up_circle,arrwo_up_circle_focused,
  send_outline,send_outline_focused,
  list,list_focused,
  profile,profile_focused

} from 'assets/icons'
// Screens
import { colors } from 'services/Theme';
import PayIn from 'screens/payin'
import ConfirmPayin from 'screens/payin/ConfirmPayin'
import PayOut from 'screens/payout'
import ConfirmPayout from 'screens/payout/ConfirmPayout'
import SendMKI from 'screens/sendmki'
import ConfirmTransfer from 'screens/sendmki/ConfirmTransfer'
import Account from 'screens/account'
import Profile from 'screens/profile'
// stacks
const Tab=createBottomTabNavigator();

// Bottom stacks
function PayInStack(){
  return (
    <Tab.Navigator screenOptions={{tabBarVisible:false}}>
      <Tab.Screen name="PayIn" component={PayIn} />
      <Tab.Screen name="ConfirmPayin" component={ConfirmPayin} /> 
    </Tab.Navigator>
  )
}
function PayOutStack(){
  return (
    <Tab.Navigator screenOptions={{tabBarVisible:false}}>
      <Tab.Screen name="PayOut" component={PayOut} />
      <Tab.Screen name="ConfirmPayout" component={ConfirmPayout} />
    </Tab.Navigator>
  )
}
function SendMKIStack(){
  return (
    <Tab.Navigator screenOptions={{tabBarVisible:false}}>
      <Tab.Screen name="SendMKI" component={SendMKI} />
      <Tab.Screen name="ConfirmTransfer" component={ConfirmTransfer} />
    </Tab.Navigator>
  )
}
function AccountStack(){
  return (
    <Tab.Navigator screenOptions={{tabBarVisible:false}}>
      <Tab.Screen name="Account" component={Account} />
    </Tab.Navigator>
  )
}

export default function BottomTab(){
  return (
    <NavigationContainer ref={navigationRef}>
    <Tab.Navigator
    tabBarOptions={{style:styles.tabBar,keyboardHidesTabBar:true}}
      initialRouteName="Account">
      <Tab.Screen
      listeners={{tabPress:({})=>Navigate("PayIn")}}
      options={{
        tabBarLabel:({focused})=><Text style={focused?{color:colors.primary}:{color:colors.grey}}>Pay In</Text>,
        tabBarIcon:({focused,color,size})=><Image source={focused?arrwo_down_circle_focused:arrwo_down_circle} resizeMode="contain" />}} 
      name="PayIn" component={PayInStack} />
      <Tab.Screen
      listeners={{tabPress:({})=>Navigate("PayOut")}} 
      options={{
        tabBarLabel:({focused})=><Text style={focused?{color:colors.primary}:{color:colors.grey}}>Pay Out</Text>,
        tabBarIcon:({focused,color,size})=><Image source={focused?arrwo_up_circle_focused:arrwo_up_circle} resizeMode="contain" />}}
      name="PayOut" component={PayOutStack} />
      <Tab.Screen 
      listeners={{tabPress:({})=>Navigate("SendMKI")}}
      options={{
        tabBarLabel:({focused})=><Text style={focused?{color:colors.primary}:{color:colors.grey}}>Send MKI</Text>,
        tabBarIcon:({focused,color,size})=><Image source={focused?send_outline_focused:send_outline} resizeMode="contain" />}}
      name="SendMKI" component={SendMKIStack} />
      <Tab.Screen 
      options={{
        tabBarLabel:({focused})=><Text style={focused?{color:colors.primary}:{color:colors.grey}}>Account</Text>,
        tabBarIcon:({focused,color,size})=><Image source={focused?list_focused:list} resizeMode="contain" />}}
      name="Account" component={AccountStack} />
      <Tab.Screen
      listeners={{tabPress:({})=>Navigate("Profile")}}
      options={{
        tabBarLabel:({focused})=><Text style={focused?{color:colors.primary}:{color:colors.grey}}>Profile</Text>,
        tabBarIcon:({focused,color,size})=><Image source={focused? profile_focused:profile} resizeMode="contain" />}}
      name="Profile" component={Profile} />
    </Tab.Navigator>
    </NavigationContainer>
  )
}

const styles=StyleSheet.create({
  tabBar:{
    backgroundColor: Appearance.getColorScheme()=="dark"?colors.black:colors.background,
    // paddingTop:15,
    height:65,
    borderTopWidth:2,
    borderColor:colors.grey,
    borderTopColor:Appearance.getColorScheme()=="dark"?colors.black10:colors.white,
    // Shadow
    shadowColor: "#000",
    shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowOpacity: 0.58,
  shadowRadius: 16.00,
  elevation: 0,
  }
})