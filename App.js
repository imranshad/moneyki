import React, { Component } from 'react';
import { SafeAreaView, StatusBar ,View, StyleSheet, Appearance} from 'react-native';
import MainNavigator from './src/navigation'
import {colors} from './src/services/Theme'

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar translucent />
      <SafeAreaView style={{flex:1}}>
        <MainNavigator />
      </SafeAreaView>
      </View>
    );
  }
}
const styles=StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:Appearance.getColorScheme()=="dark"?colors.black:colors.background
  }
})